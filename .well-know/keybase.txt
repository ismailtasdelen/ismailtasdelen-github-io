==================================================================
https://keybase.io/ismailtasdelen
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://ismailtasdelen.com
  * I am ismailtasdelen (https://keybase.io/ismailtasdelen) on keybase.
  * I have a public key ASCaIFrPEkLKju4toEASXRsNWFTZFTAJ77rTxVapIJy-RAo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01209a205acf1242ca8eee2da040125d1b0d5854d9153009efbad3c556a9209cbe440a",
      "host": "keybase.io",
      "kid": "01209a205acf1242ca8eee2da040125d1b0d5854d9153009efbad3c556a9209cbe440a",
      "uid": "9824fedc841f47a48c457173e0bb7519",
      "username": "ismailtasdelen"
    },
    "merkle_root": {
      "ctime": 1551609080,
      "hash": "258e00fe06a8442580eba9a5018f2c69ecf5f817d42aa9b46eb9636640b70375c73233fd1ad8369716b813994602657db25ab082b0e7177113f146aadf27ee1b",
      "hash_meta": "0d2c08384786228277e1e133343b6344f549d8373721dc05208187ce5a3b1c59",
      "seqno": 4866132
    },
    "service": {
      "entropy": "rCg2wru20i1fj9ZCvpHu1cZ9",
      "hostname": "ismailtasdelen.com",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "3.0.0"
  },
  "ctime": 1551609096,
  "expire_in": 504576000,
  "prev": "a598eba4961d0596666272554952e2f00f923004a5e9d4535ffae785a6dcc4d5",
  "seqno": 8,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgmiBazxJCyo7uLaBAEl0bDVhU2RUwCe+608VWqSCcvkQKp3BheWxvYWTESpcCCMQgpZjrpJYdBZZmYnJVSVLi8A+SMASl6dRTX/rnhabcxNXEILkb5gdThR/hth9w68PXO2n4S8HY5N4+BvnyljhaxumoAgHCo3NpZ8RAG+MGesbGrn1U/tmZ8zdxYU6iuD9G6oydXxI0BBZP1V/6I1rfxGpbogK1AW9wClczcCbNRZNFmi2j7KJEka/gAahzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIKHaP6uGAHYkFH9KR1suaGG/OKRV1k6TnOR24tkoRULoo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/ismailtasdelen

==================================================================
